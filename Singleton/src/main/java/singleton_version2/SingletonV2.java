package singleton_version2;

public class SingletonV2 {
    private static SingletonV2 instance = new SingletonV2();

    private SingletonV2() {

    }

    private static SingletonV2 getInstance() {
        return instance;
    }
}
