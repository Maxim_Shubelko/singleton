package singleton_version3;

public class SingletonV3 {
    private SingletonV3() {

    }

    private static class SingletonHolder {
        private final static SingletonV3 instance = new SingletonV3();
    }

    private static SingletonV3 getInstance() {
        return SingletonHolder.instance;
    }
}
