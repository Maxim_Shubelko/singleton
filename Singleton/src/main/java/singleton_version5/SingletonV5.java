package singleton_version5;

public class SingletonV5 {
    private static SingletonV5 instance;

    private SingletonV5() {

    }

    private static SingletonV5 getInstance() {
        if (instance == null) {
            synchronized (SingletonV5.class) {
                if (instance == null) {
                    instance = new SingletonV5();
                }
            }
        }
        return instance;
    }
}
