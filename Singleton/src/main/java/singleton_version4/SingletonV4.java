package singleton_version4;

public class SingletonV4 {
    private static SingletonV4 instance;

    private SingletonV4() {

    }

    private static synchronized SingletonV4 getInstance() {
        if (instance == null) {
            instance = new SingletonV4();
        }
        return instance;
    }
}
